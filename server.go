package emuxrest

import (
	"log"
	"net"
	"net/http"
	"time"

	"bitbucket.org/blacktear23/emux"
)

type Server struct {
	addr              string
	listener          net.Listener
	running           bool
	config            *emux.Config
	sessions          []*emux.Session
	handler           http.Handler
	ReadTimeout       time.Duration
	ReadHeaderTimeout time.Duration
	WriteTimeout      time.Duration
	IdleTimeout       time.Duration
	MaxHeaderBytes    int
}

var DefaultServeMux = &http.ServeMux{}

func Handle(pattern string, handler http.Handler) {
	DefaultServeMux.Handle(pattern, handler)
}

func HandleFunc(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	DefaultServeMux.HandleFunc(pattern, handler)
}

func ListenAndServe(listen string, handler http.Handler) error {
	if handler == nil {
		handler = DefaultServeMux
	}
	s, err := NewServer(listen, emux.DefaultConfig(), handler)
	if err != nil {
		return err
	}
	return s.Serve()
}

func NewServer(listen string, config *emux.Config, handler http.Handler) (*Server, error) {
	listener, err := net.Listen("tcp", listen)
	if err != nil {
		return nil, err
	}
	return &Server{
		addr:           listen,
		listener:       listener,
		config:         config,
		sessions:       []*emux.Session{},
		handler:        handler,
		MaxHeaderBytes: 1 << 20,
	}, nil
}

func (s *Server) Serve() (err error) {
	s.running = true
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			log.Println("[emuxrest] TCP Accept Error:", err)
			s.running = false
			break
		}
		session, err := emux.Server(conn, s.config)
		if err != nil {
			log.Println("[emuxrest] Session Create Error:", err)
			continue
		}
		s.sessions = append(s.sessions, session)
		go s.handleSession(session)
	}
	return
}

func (s *Server) Stop() {
	s.listener.Close()
	for i := range s.sessions {
		s.sessions[i].Close()
	}
}

func (s *Server) handleSession(sess *emux.Session) {
	hserver := &http.Server{
		Addr:              s.addr,
		Handler:           s.handler,
		ReadTimeout:       s.ReadTimeout,
		ReadHeaderTimeout: s.ReadHeaderTimeout,
		WriteTimeout:      s.WriteTimeout,
		IdleTimeout:       s.IdleTimeout,
		MaxHeaderBytes:    s.MaxHeaderBytes,
	}
	err := hserver.Serve(sess)
	if err != nil && err.Error() != "EOF" {
		log.Println("[emuxrest] HTTP Serve Error:", err)
	}
}
