package emuxrest

import (
	"io"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"bitbucket.org/blacktear23/emux"
)

var emuxPools map[string]*emux.Pool = make(map[string]*emux.Pool)
var config *emux.Config = nil
var poolSize int = 2
var DefaultClient = &http.Client{
	Transport: GetTransport(),
}
var lock sync.Mutex

func init() {
	config = emux.DefaultConfig()
}

func SetConfig(cfg *emux.Config) {
	config = cfg
}

func SetPoolSize(size int) {
	poolSize = size
}

func EmuxDial(network, addr string) (net.Conn, error) {
	var err error
	pool, have := emuxPools[addr]
	if !have {
		lock.Lock()
		pool, have = emuxPools[addr]
		if !have {
			pool, err = emux.NewPool(addr, config, poolSize)
			if err != nil {
				lock.Unlock()
				return nil, err
			}
			emuxPools[addr] = pool
		}
		lock.Unlock()
	}
	return pool.Open()
}

func GetTransport() *http.Transport {
	return &http.Transport{
		Dial: EmuxDial,
	}
}

func Get(url string) (*http.Response, error) {
	return DefaultClient.Get(url)
}

func Post(url string, contentType string, body io.Reader) (*http.Response, error) {
	return DefaultClient.Post(url, contentType, body)
}

func PostForm(url string, data url.Values) (*http.Response, error) {
	return DefaultClient.PostForm(url, data)
}

func Head(url string) (*http.Response, error) {
	return DefaultClient.Head(url)
}

func Put(url string, contentType string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest("PUT", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", contentType)
	return DefaultClient.Do(req)
}

func PutForm(url string, data url.Values) (*http.Response, error) {
	return Put(url, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
}

func Delete(url string) (*http.Response, error) {
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}
	return DefaultClient.Do(req)
}
