# emuxrest Is HTTP Wrapper Based on emux

This package is based on `net/http` package, it replace basic `TCPListener` and `Dial` object to `emux.Session` and `emux.Pool.Open`

## Install

```
go get bitbucket.org/blacktear23/emux
go get bitbucket.org/blacktear23/emuxrest
```

## Usage

Simple Server:

```
emuxrest.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, “Hello World!”)
})

emuxrest.ListenAndServe("127.0.0.1:8080", nil)
```

Simple Client:

```
resp, err := emuxrest.Get("http://127.0.0.1:8080/")
// ...
```

Intergrated with Gin

```
r := gin.Default()

r.GET("/", func(c *gin.Context){
    c.Data(200, "text/plan", []byte("Hello World!"))
})

emuxrest.ListenAndServe("127.0.0.1:8080", r)
```
