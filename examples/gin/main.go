package main

import (
	"fmt"

	"bitbucket.org/blacktear23/emuxrest"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.Any("/foo", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "OK",
			"method":  c.Request.Method,
			"path":    c.Request.URL.Path,
		})
	})

	fmt.Println("Listen: 127.0.0.1:8080")
	go emuxrest.ListenAndServe("127.0.0.1:8080", r)
	r.Run(":8081")
}
