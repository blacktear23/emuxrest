package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/blacktear23/emuxrest"
)

func main() {
	emuxrest.HandleFunc("/foo", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Method: %v, Hello, %v", r.Method, r.URL.Path)
	})
	fmt.Println("Listen: 127.0.0.1:8080")
	emuxrest.ListenAndServe("127.0.0.1:8080", nil)
}
