package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/blacktear23/emuxrest"
)

func requests() {
	processResp(emuxrest.Get("http://127.0.0.1:8080/foo"))
	processResp(emuxrest.PostForm("http://127.0.0.1:8080/foo", url.Values{"key": {"value"}}))
	processResp(emuxrest.PutForm("http://127.0.0.1:8080/foo", url.Values{"key": {"value"}}))
	processResp(emuxrest.Head("http://127.0.0.1:8080/foo"))
	processResp(emuxrest.Delete("http://127.0.0.1:8080/foo"))
}

func processResp(resp *http.Response, err error) {
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(body))
}

func main() {
	done := make(chan int)
	for i := 0; i < 100; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				requests()
			}
			done <- 1
		}()
	}
	for i := 0; i < 100; i++ {
		<-done
	}
}
